// create an new inkstance of a pixi stage
var stage = new PIXI.Stage(0xFFFFFF, true);

// create a renderer instance
var renderer = PIXI.autoDetectRenderer(400, 300);
// create a new Sprite using the texture
var graphics = new PIXI.Graphics();
var text = new PIXI.Text("");
var iter = 0;
var ay = 10;
var v0y = 0;
var sy = 0;
var dt = 0.01;

function run (){

    // add the renderer view element to the DOM
    document.body.appendChild(renderer.view);

    text.position.x = 100;
    text.position.y = 100;
    stage.addChild(text);
    stage.addChild(graphics);
    requestAnimFrame(animate);
}

function animate() {
    iter += dt;


    if(sy >= 200){
        v0y = -v0y;
    }

    var dvy = ay * dt;
    var vy = v0y + dvy;
    var dsy = 0.5 * (vy + v0y) * dt;
    sy += dsy;

    if(iter > 1.0 && iter < 1.2){
        console.log(sy);
    }

    //text.setText("v: " + v0 + "\ns: " + s + "\nds: " + ds);

    graphics.clear();
    graphics.position.x = 50;
    graphics.position.y = 50 + sy;
    graphics.lineStyle(2, 0xff0000, 1);
    graphics.drawRect(-25,-25,50,50);
    
//    graphics.rotation = iter;

    v0y = vy;
    // render the stage
    renderer.render(stage);
    requestAnimFrame(animate);
}